# AngularBestPractices

Before installing and starting this Angular 14 project, make sure your Node version is either 14.x or 16.x.

As always to run the application :

```bash
npm i && npm start
```

# Overview

The main goal of this project is to describe and implement some Angular best practices in an all-in-one application.

# Architecture best practices

- ### Modules

Separate your code in different modules by functionalities. Make sure you got one module 'commons' which will contain reused components, directives, models, etc. that does not fit in one functionnality folder. Try to make components as small as possible.

Also, make sure you import only the necessary modules to optimize the performance of your application.

- ### Lazy loading

Do lazy loading in routing with ```loadChildren()``` (see app-routing.module.ts) for better performance.

- ### Components

Use inheritance when possible to make your code more generic and reusable, for example an AlertMessagePopup component that extends from an MessagePopup component, so the MessagePopup is now extendable for any other custom popup component.

- ### Templates

Never put logic in templates, leave this responsability to the components / services.

Never use function calls in Angular template expressions, for better performance use variables (preferably observables 😊) because else the function is executed every time Angular change detection runs. Combine this practice with using the ```ChangeDetectionStrategy.OnPush``` to make sure the Angular change detection ignores changes outside the component that do not affect its input properties.

```ts
@Component({
    // ...
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExampleComponent {}
```

If you want to learn more about why you should do it, check out this very well written [medium article](https://medium.com/showpad-engineering/why-you-should-never-use-function-calls-in-angular-template-expressions-e1a50f9c0496).

- ### Services

Services must be stateless, i.e. not store data or call past transactions; they must always send the same data.

- ### Lint

Use eslint to ensures the readability and consistency of the code in your team. Don't forget to install the ESLint plugin in your IDE so you can see the warnings/errors in real-time while developing.

By default, there is only the angular-eslint recommended rules, but you can add more rules in the .eslintrc.json file. In this project I added the rxjs/recommended rules, and overrided the rxjs/finnish notation rule's setting to "error", as it is "off" in the recommended configuration. I also added the plugin "rxjs-angular" to add some rules from it.

- ### Tests

Use [Cypress](https://www.cypress.io/) to make functional tests (also called end-to-end tests).

All the Cypress tests are in the cypress/e2e folder. The configuration is in the /cypress.config.ts, if you want for example to add global env variables accessible in all your tests.

The Cypress tests are functional tests, so to write this kind of test you have to describe a use case like a user would use your application. For example, you can take a look at the cypress/e2e/todos.spec.cy.ts file where really all steps are described. For the todos form, you begin by checking if the validate button is disabled because no data is entered yet, then you enter invalid data to test that errors are actually displayed, next you type valid data, check that the button is now enabled and validate to add the todo in the list.

These tests are executed in a browser (preferably chrome), so you can **open** Cypress to see in real-time the execution of your tests, useful when writing them, with :

```sh
npm run cypress:open
```

Or you can just **run** the tests in a headless browser, really useful in a pipeline, with :

```sh
npm run cypress:run
```

And have in addition a generated video of the tests that goes in cypress/videos (so do not forget to add this folder in the .gitignore of your project).

It is a good practice to add data-cy attributes to html elements if they are not identified by an id or a class instead of adding one, to being able to manipulate them in the test. There is an example in the app/src/home/home.component.html, at the line 47 with a div that have one, used in the todos.spec.cy.ts file.

```html
<div data-cy="list-of-todos">
    ...
</div>
```

# Global best practices

- ### Always keep the project up to date

Always keep the project up to date with each new version of Angular (see the changes to be made using the site https://update.angular.io/)

- ### Constructor / ngOnInit
 
Always use NgOnInit for initializations depending on data-bound properties (@Input).

Otherwise, use the constructor for dependency injection and all other variable initialization.

- ### Types and visibility

**Never** use the "any" type, always use typed variables. Do not hesitate to create interfaces, like in the src/app/commons/models/todo.ts file where there is the interface ```Todo``` used to retrieve todos from an API, to use in a reactive form, etc.

Also, always specify the return types (except for the constructor) and the access modifiers of methods. As a reminder, in Typescript, if no access modifier is specified it is public by default ; but it is a good practice to always write the access modifier to ensure that the method encapsulation is clearly specified. For example :

```ts
private methodName(): string {
    return 'example';
}
```

- ### Use Proper Keywords (const vs let)

When you declare a variable if the value is not reassigned “Const” can be used to declare the value. The “let” keyword declares an input variable referenced within the template. Using these keywords makes the declaration of a variable more clear. With this, the code crispness and clarity are improvised with clean code.

- ### Forms

Prefer to use reactive forms better than template driven forms.

There is an example in src/app/home/home.component.ts, where it uses the rxweb library to use an interface to strongly type the form data. But if you use Angular version >= 14, it might be outdated since Angular released a strictly typed reactive form.

- ### Styles

Use [SASS](https://sass-lang.com/guide) ! As a preprocessor scripting language, it allows you to use variables, nesting and mixins. Try to organise your classes by hierarchy used in your template.

All styles must be in the scss file. If you want to applicate a style in the component.ts file, add a class to an element instead of adding style directly.

If you want to apply styles directly to your component, use the ```:host``` selector, except when your component encapsulation is assigned to ```ViewEncapsulation.None``` ; in this case, use directly the component selector, for example ```app-example-component```.

```scss
/* With ViewEncapsulation.Emulated (default), use the :host selector */
:host {
    display: flex;
}
/* With ViewEncapsulation.None, use the component selector */
app-example-component {
    display: flex;
}
```

Also, use [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) as much as possible. There is a fun little game called [Flexbox Froggy](https://flexboxfroggy.com/#fr) where you can train your flexbox skills by placing frogs above their respective lily pads. That covers the basics so it is really useful to understand what you can do with it.

If you have a if/else condition in your template, something like this :

```html
<div *ngIf="condition"><h1>True</h1></div>

<div *ngIf="!condition"><h1>False</h1></div>
```

Prefer using ng-template like the following :

```html
<div *ngIf="condition; else falseTemplate">
    <h1>True</h1>
</div>

<ng-template #falseTemplate>
    <div><h1>False</h1></div>
</ng-template>
```
In addition, ng-template allows you to reuse some part of code, useful in some cases. You can see a concrete example in src/app/home/home.component.html at line 44. 

# RxJS best practices

- ### Use RxJS !
- ### Code reactively

Have a reactive approach, i.e. use observables as much as possible (learn more on [RxJS](https://rxjs.dev/))

- ### Types

The most used types are observable, subject, behaviorSubject, replaySubject.

- ### Operators

The most used rxjs operators are WithLatestFrom, CombineLatestWith, mergeMap, switchMap, tap map, catchError, filter, of, debounceTime, takeUntil.

- ### Avoid leak memory

Always unsubscribe from subscribed observables to avoid leak memory using OnDestroy and takeUntil() operator. For example :

```ts
class ExampleComponent implements OnDestroy {
    private destroy$ = new Subject<void>();

// ...
    obs1$.pipe(
        takeUntil(this.destroy$)
    ).subscribe();

    obs2$.pipe(
        takeUntil(this.destroy$)
    ).subscribe();
// ...

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
```

You can see an example of that implementation in src/app/commons/components/details.component.ts, but in most cases you can manage to use the async pipe, which automatically unsusbscribe from your observable and make the code much clearer.

```html
<p *ngIf="(user$ | async) as user">
    Welcome {{ user.name }}
</p>
```

So when you think observable, you think pipe async. And if you really can't use the pipe async, for example to manipulate exclusively a DOM event, always use the takeUntil operator before subscribing to make sure the observable is completed when the component is destroyed.

To assure this, adding the eslint rule "rxjs-angular/prefer-takeuntil" is a good practice. You can also add the rule "rxjs-angular/prefer-async-pipe" to forbid the use of subscribe(), but it might be too extreme. In this project I added it but at a "warn" level.

- ### Forbid nested subscribes

Now that you know how subscribing manually to observables is reserved for exceptional use cases only, imagine how forbidden is a subscribe in a subscribe.

If you end up with a subscribe in a subscribe, that means you are not thinking **functionally**, you're thinking **procedurally**. You have to manipulate a stream or combine it with others, and consume that stream with a subscriber. To get rid of nested subscribes, check the [combineLatestWith](https://rxjs.dev/api/operators/combineLatestWith), [switchMap](https://www.learnrxjs.io/learn-rxjs/operators/transformation/switchmap), [mergeMap](https://www.learnrxjs.io/learn-rxjs/operators/transformation/mergemap) and [forkJoin](https://www.learnrxjs.io/learn-rxjs/operators/combination/forkjoin) operators, that will often do the job.

- ### Finnish notation

Use the finnish notation, that means adding the $ suffix to an observable variable name. This is a great practice because this way observables are easy to identify. For example :

```ts
private users$ = new Observable<User[]>();
```

# List of examples

1. Fetch from an API
2. Get route params
3. Reactive form
4. Child component
