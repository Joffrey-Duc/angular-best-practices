import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styles: [`.main {display: flex; justify-content: center; padding: 1rem;} .toolbar{position:sticky; top: 0}`],
  template: `
    <mat-toolbar class="toolbar" color="primary">
      <button mat-icon-button>
        <img src="../favicon.ico" alt="icon">
      </button>
      <span>Example App</span>
    </mat-toolbar>

    <div class="main">
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {}
