import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IFormBuilder, IFormGroup } from '@rxweb/types';
import { map, Observable, of, Subject, switchMap, withLatestFrom } from 'rxjs';
import { Todo } from '../commons/models/todo';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent {

  public click$ = new Subject<Todo>();
  public listOfTodos$: Observable<Todo[]>;
  public formGroup: IFormGroup<Todo>;
  public title: string = 'angular-best-practices';
  public todosFromApi$: Observable<Todo[]>;
  public todoFromApi$: Observable<Todo>;
  private formBuilder: IFormBuilder;

  public constructor(
    // Since Angular 14 the FormBuilder is called UntypedFormBuilder
    private fb: UntypedFormBuilder,
    private homeService: HomeService,
    private route: ActivatedRoute
  ) {
    this.formBuilder = fb;

    this.formGroup = this.formBuilder.group<Todo>({
      userId: [0, Validators.min(0)],
      id: [0, Validators.min(0)],
      title: ['', Validators.minLength(2)],
      completed: false
    });

    this.todosFromApi$ = this.homeService.getTodosFromApi$();
    this.todoFromApi$ = this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('todoId');
        if (id) {
          return this.homeService.getTodoFromApi$(id);
        }
        return of({} as Todo);
      })
    );

    const arrayOfTodos$ = of([] as Todo[]);

    this.listOfTodos$ = this.click$.pipe(
      withLatestFrom(arrayOfTodos$),
      map(([todoToAdd, arrayOfTodos]) => {
        arrayOfTodos.push(todoToAdd);
        return arrayOfTodos;
      })
    );
  }

}
