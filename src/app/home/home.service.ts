import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, switchMap, tap } from 'rxjs';
import { Todo } from '../commons/models/todo';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpClient: HttpClient) {}

  public getTodoFromApi$(todoId: number | string): Observable<Todo> {
    return this.httpClient.get<Todo>(`https://jsonplaceholder.typicode.com/todos/${todoId}`).pipe(
      catchError((error: unknown) => {
        console.error(`Error retrieving a todo from API : ${error}`);
        return of({} as Todo);
      })
    );
  }

  public getTodosFromApi$(): Observable<Todo[]> {
    return this.httpClient.get<Todo[]>('https://jsonplaceholder.typicode.com/todos/').pipe(
      catchError((error: unknown) => {
        console.error(`Error retrieving a todo from API : ${error}`);
        return of([] as Todo[]);
      })
    );
  }

}
