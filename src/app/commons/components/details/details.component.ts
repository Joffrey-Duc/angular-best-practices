import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostBinding, OnDestroy, ViewChild } from '@angular/core';
import { fromEvent, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailsComponent implements OnDestroy {

  @HostBinding('class.highlight') public highlight: boolean = false;
  private destroy$ = new Subject<void>();

  constructor(
    private changeDetection: ChangeDetectorRef,
    private elementRef: ElementRef<HTMLElement>
  ) {
    const mouseenter$ = fromEvent(this.elementRef.nativeElement, 'mouseenter');
    const mouseleave$ = fromEvent(this.elementRef.nativeElement, 'mouseleave');
    
    mouseenter$.pipe(
      takeUntil(this.destroy$)
    // eslint-disable-next-line rxjs-angular/prefer-async-pipe
    ).subscribe(() => {
      console.log('mouseenter');
      this.highlight = true;
      this.changeDetection.markForCheck();
    });

    mouseleave$.pipe(
      takeUntil(this.destroy$)
    // eslint-disable-next-line rxjs-angular/prefer-async-pipe
    ).subscribe(() => {
      console.log('mouseleave');
      this.highlight = false;
      this.changeDetection.markForCheck();
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
