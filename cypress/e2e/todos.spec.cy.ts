describe('Todos test', () => {

  // Cypress.env() get the key in the env object located in the file cypress.config.ts
  const todoId = Cypress.env('todoId');

  it('shoud load the home page', () => {
    cy.visit('/home');
    cy.contains('Example App');
    cy.contains('List of examples');
  });

  it('shoud load the json list of todos', () => {
    cy.visit('/home');
    cy.get('ul.json-list li')
      .should('exist')
      .and('have.length', 200);
  });

  it('should get route params and retrieve a single todo', () => {
    cy.visit('/home');
    cy.get('p[data-cy="single-todo"]')
      .should('have.text', '{}');

    cy.visit(`/home/todos/${todoId}`);
    cy.get('p[data-cy="single-todo"]')
      .should('not.have.text', '{}')
      .and('contain.text', `"id": ${todoId}`);
  });

  it('should trigger the form\'s errors and add a valid todo', () => {
    cy.visit('/home');
    // Button should be disabled and the list of todos should be empty
    cy.get('form button:contains(Add to list)')
      .should('exist')
      .and('be.disabled');
    cy.get('div[data-cy="list-of-todos"]').should('contain.text', 'No todo yet');

    // Trigger errors on inputs
    cy.get('form input[formcontrolname="userId"]').type('{selectall}-1');
    cy.get('form button:contains(Add to list)').click({force: true});
    cy.get('form mat-error:contains(UserId must be a positive number)').should('exist');

    cy.get('form input[formcontrolname="id"]').type('{selectall}-1');
    cy.get('form button:contains(Add to list)').click({force: true});
    cy.get('form mat-error:contains(Id must be a positive number)').should('exist');

    cy.get('form input[formcontrolname="title"]').type('{selectall}T');
    cy.get('form button:contains(Add to list)').click({force: true});
    cy.get('form mat-error:contains(Title must have at least 2 characters)').should('exist');

    // Type valid data and add the todo to the list
    cy.get('form input[formcontrolname="userId"]').type('{selectall}1');
    cy.get('form input[formcontrolname="id"]').type('{selectall}1');
    cy.get('form input[formcontrolname="title"]').type('{selectall}Example title');
    cy.get('form mat-slide-toggle[formcontrolname="completed"] input').should('not.be.checked');
    cy.get('form button:contains(Add to list)')
      .should('be.enabled')
      .click();

    // List of todos should not be empty anymore and should contain the added todo
    cy.get('div[data-cy="list-of-todos"]')
      .should('not.contain.text', 'No todo yet');
    cy.get('div[data-cy="list-of-todos"] ul li')
      .should('have.length', 1)
      .and('contain.text', '"id": 1');
  });

  it('should highlight the app-details component when hovered', () => {
    cy.visit('/home');
    cy.get('app-details')
      .should('exist')
      .trigger('mouseenter')
      .should('have.class', 'highlight');
  });
})
