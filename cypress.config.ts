import { defineConfig } from 'cypress'

export default defineConfig({
  env: {
    'todoId': 1
  },
  e2e: {
    'baseUrl': 'http://localhost:4200',
  }
})
